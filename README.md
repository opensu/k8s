<img height="24" src='https://img.shields.io/badge/Ubuntu-20.04%20LTS-E95420?style=for-the-badge&logo=ubuntu&logoColor=F5F5F5'> 
<img height="24" src="https://img.shields.io/badge/kubernetes-v1.31-326CE5?style=for-the-badge&logo=kubernetes&logoColor=F5F5F5" alt="Kubernetes">
<img height="24" src="https://img.shields.io/badge/CRI-containerd-000000?style=for-the-badge&logo=containerd&logoColor=F5F5F5" alt="containerd">
<img height="24" src="https://img.shields.io/badge/CNI-cilium-F8C517?style=for-the-badge&logo=cilium&logoColor=F5F5F5" alt="cilium">

[toc]

## 准备培训环境

参考： [Kubernetes 文档](https://kubernetes.io/zh/docs/) / [入门](https://kubernetes.io/zh/docs/setup/) / [生产环境](https://kubernetes.io/zh/docs/setup/production-environment/) / [使用部署工具安装 Kubernetes](https://kubernetes.io/zh/docs/setup/production-environment/tools/) /  [使用 kubeadm 引导集群](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/) / [安装 kubeadm](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)
<hr>
<img align="middle" src="https://gitlab.com/opensu/k8s/-/raw/main/excalidraw/kubeadm.svg">


## B. [准备开始](https://kubernetes.io/zh-cn/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#%E5%87%86%E5%A4%87%E5%BC%80%E5%A7%8B)

- 一台兼容的 Linux 主机。Kubernetes 项目为基于 Debian 和 Red Hat 的 Linux 发行版以及一些不提供包管理器的发行版提供通用的指令
- 每台机器 2 GB 或更多的 RAM（如果少于这个数字将会影响你应用的运行内存）

- CPU 2 核心及以上

- 集群中的所有机器的网络彼此均能相互连接（公网和内网都可以）

- 节点之中不可以有重复的主机名、MAC 地址或 product_uuid。请参见[这里](https://kubernetes.io/zh-cn/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#verify-mac-address)了解更多详细信息。

- 开启机器上的某些端口。请参见[这里](https://kubernetes.io/zh-cn/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports)了解更多详细信息。

- 禁用交换分区。为了保证 kubelet 正常工作，你必须 禁用交换分区

  - 例如，`sudo swapoff -a` 将暂时禁用交换分区。要使此更改在重启后保持不变，请确保在如 `/etc/fstab`、`systemd.swap` 等配置文件中禁用交换分区，具体取决于你的系统如何配置


## U. [确保每个节点上 MAC 地址和 product_uuid 的唯一性](https://kubernetes.io/zh-cn/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#verify-mac-address) 

- 你可以使用命令 `ip link` 或 `ifconfig -a` 来获取网络接口的 MAC 地址
- 可以使用 `sudo cat /sys/class/dmi/id/product_uuid` 命令对 product_uuid 校验

一般来讲，硬件设备会拥有唯一的地址，但是有些虚拟机的地址可能会重复。 Kubernetes 使用这些值来唯一确定集群中的节点。 如果这些值在每个节点上不唯一，可能会导致安装[失败](https://github.com/kubernetes/kubeadm/issues/31)


## V. <img width='144' src="https://www.vmware.com/content/dam/digitalmarketing/vmware/en/images/company/vmware-logo-grey.svg">虚拟机  

> <kbd>新建...</kbd> /  
> &nbsp;&nbsp;&nbsp;&nbsp;<kbd>创建自定虚拟机</kbd> /  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		<kbd>Linux</kbd> / `Ubuntu 64位`

- 设置过程

|  ID  |   『虚拟机』设置   |        建议配置        | 默认值  |     说明     |
| :--: | :--------: | :--------------------: | :-----: | :----------: |
|  1   |   处理器   |           -            |    2    |   最低要求   |
|  2   |    内存    |           -            | 4096 MB |   节约内存   |
|  3   |   显示器   | 取消复选`加速 3D 图形` |  复选   |   节约内存   |
|  4.1 | 网络适配器1 |           -            |   nat   |    需上网    |
|  4.2 | 网络适配器2 |           -            |   host only   |    固定IP    |
|  5   |    硬盘    |         `40` GB         |  20 GB  | 保证练习容量 |
|  6   |    选择固件类型  |         UEFI         |  传统 BIOS  | VMware Fusion 支持嵌套虚拟化 |

<img src="https://gitlab.com/opensu/k8s/-/raw/main/images/efi.png">

- 设置结果

|  ID  | HOSTNAME |    CPU 核    |      RAM       |   DISK    |   NIC   |
| :--: | :------------------: | :----------: | :------------: | :-------: | :-----: |
|  1   |     `k8s-master`     | 2 或更多 | 2 GB或更多 | 40 GB | 1. nat<br>2. host only |
|  2   |    `k8s-worker1`    |     同上     | 同上 |   同上    |  同上   |
|  3   |    `k8s-worker2`    |     同上     |      同上      |   同上    |  同上   |

## I. 安装 Ubuntu 20.04 LTS
🖥️ **x86**<br>      🔗 https://mirrors.tencent.com/ubuntu-releases/20.04/ubuntu-20.04.6-live-server-amd64.iso

🖥️ **arm64**<br>      🔗 https://mirrors.tencent.com/ubuntu-cdimage/releases/20.04/release/ubuntu-20.04.5-live-server-arm64.iso

<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >1. Willkommen! Bienvenue! Welcome! Welkom!</div>

   &nbsp;&nbsp;&nbsp;&nbsp;[ `English` ]  
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/1ubuntu-welcome.png)
    
<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >2. Installer update available</div>

   &nbsp;[ `Continue without updating` ]  
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/2ubuntu-update.png)


<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >3. Keyboard configuration</div>

   [ `Done` ]  
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/3ubuntu-keyboard.png)


<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >4. Network connections</div>

   [ `Done` ]  
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/4ubuntu-network.png)
    
<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >5. Configure proxy</div>

   [ `Done` ]  
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/5ubuntu-proxy.png)
    
<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >6. Configure Ubuntu archive mirror</div>

   - 🖥️ **x86**<br>🔗 Mirror address: https://mirrors.tencent.com/ubuntu  / [ `Done` ]  

   - 🖥️ **arm64**<br>🔗 Mirror address: https://mirrors.tencent.com/ubuntu-ports  / [ `Done` ]  

   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/6ubuntu-mirror.png)
    
<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >7. Guided storage configuration</div>

   [ `Done` ]  
    
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/7ubuntu-storage.png)
    

<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >8. Storage configuration</div>

   [ `Done` ]  
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/8ubuntu-storage1.png)
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/8ubuntu-storage2.png)

<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >9. Profile setup</div>

   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your name: `vagrant`  
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your server 's name: `k8s-master`  
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pick a username: `vagrant`  
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Choose a password: `vagrant`  
   &nbsp;&nbsp;&nbsp;&nbsp;Confirm your password: `vagrant`  
   &nbsp;&nbsp;&nbsp;&nbsp;/ [ `Done` ]  
    
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/9ubuntu-profile.png)
    
<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >10. SSH Setup</div>

   [`X`] Install OpenSSH server  
    / [ `Done` ]  
   ![](http://k8s.ruitong.cn:8080/K8s/images/10ubuntu-ssh.png)

<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >11. Featured Server Snaps</div>

   [ `Done` ]  
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/11ubuntu-snaps.png)

<div style="background: #E8541F; padding: 6px 12px; font-weight: bold; display: block; color: #fff; margin: -12px; margin-bottom: -12px; margin-bottom: 12px;" >12. Install complete!</div>

   🅰️ [ `Cancel update and reboot` ]  
    
   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/12ubuntu-complete.png)
    

🅱️ [ `Reboot Now` ]  

   ![](https://gitlab.com/opensu/k8s/-/raw/main/images/12ubuntu-complete1.png)
    
13. 建议（可选）

    关机后，做个快照

## P. 准备工作

### 1. <strong style='color: #1A97D5'>[选做] 设置当前用户 sudo 免密</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

   > 不想每次都输入密码 - 加速

   ```bash
# 当前用户的密码
export USER_PASS=vagrant

# 缓存 sudo 密码 vagrant
echo ${USER_PASS} | sudo -v -S 

# 永久生效
sudo tee /etc/sudoers.d/$USER <<-EOF
$USER ALL=(ALL) NOPASSWD: ALL
EOF
   
sudo cat /etc/sudoers.d/$USER

   ```

### 2.<strong style='color: #1A97D5'> [选做] 设置 root 密码</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
export USER_PASS=vagrant

(echo ${USER_PASS}; echo ${USER_PASS}) \
  | sudo passwd root

sudo sed -i /etc/ssh/sshd_config \
  -e '/PasswordAuthentication /{s+#++;s+no+yes+}'

if egrep -q '^(#|)PermitRootLogin' /etc/ssh/sshd_config; then
  echo sudo sed -i /etc/ssh/sshd_config \
    -e '/^#PermitRootLogin/{s+#++;s+ .*+ yes+}' \
    -e '/^PermitRootLogin/{s+#++;s+ .*+ yes+}' 
fi

sudo systemctl restart sshd

```

### 3.<strong style='color: #1A97D5'> [选做] 设置时区</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

   ```bash
sudo timedatectl set-timezone Asia/Shanghai

   ```

### 4.<strong style='color: #1A97D5'> [选做] 扩容</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# 逻辑卷名
export LVN=$(sudo lvdisplay | awk '/Path/ {print $3}')

echo -e " LV Name: \e[1;34m${LVN}\e[0;0m"

# 扩容
sudo lvextend -r -l 100%PVS $LVN

df -h / | egrep [0-9]+G

```
### 5.<strong style='color: #1A97D5'> [选做] 更新软件包，默认接受重启服务</strong> <img height='36pix' src='https://img.shields.io/badge/Ubuntu-22.04%20LTS-E95420?style=for-the-badge&logo=ubuntu&logoColor=F5F5F5'>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
if [ "$(lsb_release -cs)" = "jammy" ]; then
  # Which services should be restarted?
  export NFILE=/etc/needrestart/needrestart.conf

  sudo sed -i $NFILE \
    -e '/nrconf{restart}/{s+i+a+;s+#++}'

  grep nrconf{restart} $NFILE
fi

```

### 6.<strong style='color: #1A97D5'> [选做] 使用国内镜像仓库</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# C. 国内
if ! curl --connect-timeout 2 www.google.com &>/dev/null; then
  if [ "$(uname -m)" = "aarch64" ]; then
    # arm64
    export MIRROR_URL=http://mirrors.tencent.com/ubuntu-ports
  else
    # amd64
    export MIRROR_URL=http://mirrors.tencent.com/ubuntu
  fi
  
  # 生成软件仓库源
  export CODE_NAME=$(lsb_release -cs)
  export COMPONENT="main restricted universe multiverse"
  sudo tee /etc/apt/sources.list >/dev/null <<-EOF
deb $MIRROR_URL $CODE_NAME $COMPONENT
deb $MIRROR_URL $CODE_NAME-updates $COMPONENT
deb $MIRROR_URL $CODE_NAME-backports $COMPONENT
deb $MIRROR_URL $CODE_NAME-security $COMPONENT
EOF
fi

cat /etc/apt/sources.list

```

### 7.<strong style='color: #1A97D5'> <必做> 设置静态 IP</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# 获取 IP
export NICP=$(ip a | awk '/inet / {print $2}' | grep -v ^127)

if [ "$(echo ${NICP} | wc -w)" != "1" ]; then
  select IP1 in ${NICP}; do
    break
  done
else
  export IP1=${NICP}
fi

:<<EOF
在出现的选择菜单号，选择 6.7
  #1) 192.168.8.136/24
  #2) 192.168.6.7/24
  #? '2'
EOF
```
```bash
# 获取网卡名 - 使用第2块网卡
export NICN=$(ip a | awk '/^3:/ {print $2}' | sed 's/://')

# 获取网关
export NICG=$(ip route | awk '/^default/ {print $3}')

# 获取 DNS
unset DNS; unset DNS1
for i in 114.114.114.114 8.8.8.8; do
  if nc -w 2 -zn $i 53 &>/dev/null; then
    export DNS1=$i
    export DNS="$DNS, $DNS1"
  fi
done

printf "
  addresses: \e[1;34m${IP1}\e[0;0m
  ethernets: \e[1;34m${NICN}\e[0;0m
  routes: \e[1;34m${NICG}\e[0;0m
  nameservers: \e[1;34m${DNS#, }\e[0;0m
"

```
```bash
# 更新 dns
sudo sed -i /etc/systemd/resolved.conf \
  -e '/^DNS=/s/=.*/='"$(echo ${DNS#, } | sed 's/,//g')"'/'
sudo systemctl restart systemd-resolved
sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf

# 更新网卡配置文件 - 第2块网卡 静态ip
export NYML=/etc/netplan/50-vagrant.yaml
sudo tee ${NYML} <<EOF
network:
  ethernets:
    ${NICN}:
      addresses: [${IP1}]
      routes:
        - to: default
          via: ${NICG}
  version: 2
  renderer: networkd
EOF

sudo netplan apply

```

### 8.<strong style='color: #1A97D5'> <必做> 安装相关软件</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# -	远程, ssh 免交互, 编辑文件, storageClass
# -	Tab 自动补全, nc, ping

curl -s http://mirror.nju.edu.cn/ubuntu/gpg-public.key | sudo apt-key add -

sudo apt-get update && \
sudo apt-get -y install \
  openssh-server sshpass vim nfs-common \
  bash-completion netcat-openbsd iputils-ping

```


### 9.<strong style='color: #1A97D5'> <必做> 禁用 swap</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
## 为了保证kubelet正常工作,你必须禁用交换分区
# 取交换文件名称
export SWAPF=$(awk '/swap/ {print $1}' /etc/fstab)

# 立即禁用
sudo swapoff $SWAPF

# 永久禁用
sudo sed -i '/swap/d' /etc/fstab

# 删除交换文件
sudo rm $SWAPF

# 确认
free -h

```


### 10.<strong style='color: #1A97D5'> <必做> 安装运行时</strong> 
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
## 创建镜像仓库文件
export AFILE=/etc/apt/sources.list.d/docker.list

if ! curl --connect-timeout 2 www.google.com &>/dev/null; then
  # C. 国内
  export AURL=https://mirrors.tencent.com/docker-ce
else
  # A. 国外
  export AURL=http://download.docker.com
fi

sudo tee $AFILE >/dev/null <<-EOF
deb $AURL/linux/ubuntu $(lsb_release -cs) stable
EOF

# 导入公钥
# http://download.docker.com/linux/ubuntu/gpg
curl -s https://mirrors.tencent.com/docker-ce/linux/ubuntu/gpg \
  | sudo apt-key add -

# 安装 containerd
sudo apt-get update && \
sudo apt-get -y install containerd.io

```
```bash
# 生成默认配置文件
containerd config default \
  | sed -e '/SystemdCgroup/s+false+true+' \
   -e "/sandbox_image/s+3.6+3.10+" \
  | sudo tee /etc/containerd/config.toml

# mirror
if ! curl --connect-timeout 2 www.google.com &>/dev/null; then
  # C. 国内
  #    - docker.io
  sudo mkdir -p /etc/containerd/certs.d/docker.io
  sudo tee -a /etc/containerd/certs.d/docker.io/hosts.toml >/dev/null<<-EOF
[host."https://0a34ef3ff5000f620f00c0016cb57320.mirror.swr.myhuaweicloud.com"]
capabilities = ["pull", "resolve", "push"]
EOF
  #    - registry.k8s.io
  sudo mkdir -p /etc/containerd/certs.d/registry.k8s.io
  sudo tee -a /etc/containerd/certs.d/registry.k8s.io/hosts.toml >/dev/null<<-EOF
[host."https://k8s.linkos.org"]
capabilities = ["pull", "resolve", "push"]
EOF

  sudo sed -i /etc/containerd/config.toml \
  -e "/sandbox_image/s+registry.k8s.io+registry.aliyuncs.com/google_containers+" \
  -e '/config_path/s+""+"/etc/containerd/certs.d"+'
fi

# 服务重启
sudo systemctl restart containerd

```

## K. 安装 K8s

### 11.<strong style='color: #1A97D5'> <必做> 安装 kubeadm、kubelet 和 kubectl</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
## 添加 Kubernetes apt 仓库
sudo mkdir /etc/apt/keyrings &>/dev/null

if ! curl --connect-timeout 2 www.google.com &>/dev/null; then
  # C. 国内
  export AURL=https://mirrors.tencent.com/kubernetes_new/core:/stable:/v1.31/deb
else
  # F. 国外
  export AURL=http://pkgs.k8s.io/core:/stable:/v1.31/deb
fi

export KFILE=/etc/apt/keyrings/kubernetes-apt-keyring.gpg
curl -sL ${AURL}/Release.key \
    | sudo gpg --dearmor -o ${KFILE}

sudo tee /etc/apt/sources.list.d/kubernetes.list <<-EOF
deb [signed-by=${KFILE}] ${AURL} /
EOF

sudo apt-get -y update

```
> 官方考试版本-CKA  
>     https://training.linuxfoundation.cn/certificates/1  
>  
> 官方考试版本-CKS  
>     https://training.linuxfoundation.cn/certificates/16  
>  
> 官方考试版本-CKAD  
>     https://training.linuxfoundation.cn/certificates/4

```bash
# 列出所有小版本
sudo apt-cache madison kubelet | grep 1.31

# 更新 apt 包索引并安装使用 Kubernetes apt 仓库所需要的包
# 安装 kubelet、kubeadm 和 kubectl 考试版本
sudo apt-get -y install \
  apt-transport-https ca-certificates curl gpg \
  kubelet=1.31.1-* kubeadm=1.31.1-* kubectl=1.31.1-*

# 锁定版本
sudo apt-mark hold kubelet kubeadm kubectl

```



### 12.<strong style='color: #1A97D5'> [建议] kubeadm 命令补全</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# 立即生效
source <(kubeadm completion bash)

# 永久生效
[ ! -d /home/${LOGNAME}/.kube ] && mkdir /home/${LOGNAME}/.kube

kubeadm completion bash \
  | tee /home/${LOGNAME}/.kube/kubeadm_completion.bash.inc \
  | sudo tee /root/.kube/kubeadm_completion.bash.inc

echo 'source ~/.kube/kubeadm_completion.bash.inc' \
  | tee -a /home/${LOGNAME}/.bashrc \
  | sudo tee -a /root/.bashrc

```



### 13.<strong style='color: #1A97D5'> <必做> crictl 命令配置</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# 配置文件
sudo crictl config \
  --set runtime-endpoint=unix:///run/containerd/containerd.sock \
  --set image-endpoint=unix:///run/containerd/containerd.sock \
  --set timeout=10

# crictl_tab
source <(crictl completion bash)

sudo test ! -d /root/.kube && sudo mkdir /root/.kube

crictl completion bash \
  | tee /home/${LOGNAME}/.kube/crictl_completion.bash.inc \
  | sudo tee /root/.kube/crictl_completion.bash.inc

echo 'source ~/.kube/crictl_completion.bash.inc' \
  | tee -a /home/${LOGNAME}/.bashrc \
  | sudo tee -a /root/.bashrc

# 注销重新登陆后,生效
sudo usermod -aG root ${LOGNAME}

```



### 14.<strong style='color: #1A97D5'> <必做> kubeadm 命令使用前检查</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
## 1. Bridge
# [ERROR FileContent-.proc-sys-net-bridge-bridge-nf-call-iptables]: /proc/sys/net/bridge/bridge-nf-call-iptables does not exist
sudo apt-get -y install bridge-utils && \
echo br_netfilter \
  | sudo tee /etc/modules-load.d/br.conf && \
sudo modprobe br_netfilter

```

```bash
## 2. 内核支持
# [ERROR FileContent-.proc-sys-net-ipv4-ip_forward]: /proc/sys/net/ipv4/ip_forward contents are not set to 1
sudo tee /etc/sysctl.d/k8s.conf <<-EOF
net.ipv4.ip_forward=1
EOF

# 立即生效
sudo sysctl -p /etc/sysctl.d/k8s.conf

```



### 15.<strong style='color: #1A97D5'> <必做> 配置 主机名</strong>  

💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font>]$**

```bash
sudo hostnamectl set-hostname k8s-master
```

🖥️ **[vagrant@<font style="color: #0C62B0">k8s-node1</font>]$**
```bash
sudo hostnamectl set-hostname k8s-node1
```
🖥️ **[vagrant@<font style="color: #0C62B0">k8s-node2</font>]$**
```bash
sudo hostnamectl set-hostname k8s-node2
```

### 16.<strong style='color: #1A97D5'> <必做> 编辑 hosts</strong>
💻 🖥️ 🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# 显示 ip 地址和主机名，方便复制
echo $(hostname -I) $(hostname)

```

```bash
sudo tee -a /etc/hosts >/dev/null <<EOF
# K8s-cluster
```
```bash
192.168.6.7 k8s-master
192.168.6.8 k8s-node1
192.168.6.9 k8s-node2
```
```bash
EOF

cat /etc/hosts

```



### 17.<strong style='color: #1A97D5'> <必做> kubeadm init 初始化</strong>

💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font>]$**

```bash
export REGISTRY_MIRROR=registry.aliyuncs.com/google_containers
export POD_CIDR=172.16.1.0/16
export SERVICE_CIDR=172.17.1.0/18

if ! curl --connect-timeout 2 www.google.com &>/dev/null; then
  # C. 国内
  sudo kubeadm config images pull \
    --kubernetes-version 1.31.1 \
    --image-repository ${REGISTRY_MIRROR}
  sudo kubeadm init \
    --kubernetes-version 1.31.1 \
    --apiserver-advertise-address=${IP1%/*} \
    --pod-network-cidr=${POD_CIDR} \
    --service-cidr=${SERVICE_CIDR} \
    --node-name=$(hostname -s) \
    --image-repository=${REGISTRY_MIRROR}
else
  # A. 国外
  sudo kubeadm config images pull \
    --kubernetes-version 1.31.1
  sudo kubeadm init \
    --kubernetes-version 1.31.1 \
    --apiserver-advertise-address=${IP1%/*} \
    --pod-network-cidr=${POD_CIDR} \
    --service-cidr=${SERVICE_CIDR} \
    --node-name=$(hostname -s)
fi

```

> [init] Using Kubernetes version: v1.31.1  
> [preflight] Running pre-flight checks  
> 	[WARNING FileExisting-socat]: socat not found in system path  
> [preflight] Pulling images required for setting up a Kubernetes cluster  
> [preflight] This might take a minute or two, depending on the speed of your internet connection  
> [preflight] You can also perform this action beforehand using 'kubeadm config images pull'  
> W0216 03:13:27.254673   25288 checks.go:846] detected that the sandbox image "registry.aliyuncs.com/google_containers/pause:3.8" of the container runtime is inconsistent with that used by kubeadm.It is recommended to use "registry.aliyuncs.com/google_containers/pause:3.10" as the CRI sandbox image.  
> [certs] Using certificateDir folder "/etc/kubernetes/pki"  
> [certs] Generating "ca" certificate and key  
> [certs] Generating "apiserver" certificate and key  
> [certs] apiserver serving cert is signed for DNS names [k8s-master kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [172.17.0.1 192.168.8.130]  
> [certs] Generating "apiserver-kubelet-client" certificate and key  
> [certs] Generating "front-proxy-ca" certificate and key  
> [certs] Generating "front-proxy-client" certificate and key  
> [certs] Generating "etcd/ca" certificate and key  
> [certs] Generating "etcd/server" certificate and key  
> [certs] etcd/server serving cert is signed for DNS names [k8s-master localhost] and IPs [192.168.8.130 127.0.0.1 ::1]  
> [certs] Generating "etcd/peer" certificate and key  
> [certs] etcd/peer serving cert is signed for DNS names [k8s-master localhost] and IPs [192.168.8.130 127.0.0.1 ::1]  
> [certs] Generating "etcd/healthcheck-client" certificate and key  
> [certs] Generating "apiserver-etcd-client" certificate and key  
> [certs] Generating "sa" key and public key  
> [kubeconfig] Using kubeconfig folder "/etc/kubernetes"  
> [kubeconfig] Writing "admin.conf" kubeconfig file  
> [kubeconfig] Writing "super-admin.conf" kubeconfig file  
> [kubeconfig] Writing "kubelet.conf" kubeconfig file  
> [kubeconfig] Writing "controller-manager.conf" kubeconfig file  
> [kubeconfig] Writing "scheduler.conf" kubeconfig file  
> [etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"  
> [control-plane] Using manifest folder "/etc/kubernetes/manifests"  
> [control-plane] Creating static Pod manifest for "kube-apiserver"  
> [control-plane] Creating static Pod manifest for "kube-controller-manager"  
> [control-plane] Creating static Pod manifest for "kube-scheduler"  
> [kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"  
> [kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"  
> [kubelet-start] Starting the kubelet  
> [wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests"  
> [kubelet-check] Waiting for a healthy kubelet at http://127.0.0.1:10248/healthz. This can take up to 4m0s  
> [kubelet-check] The kubelet is healthy after 502.102846ms  
> [api-check] Waiting for a healthy API server. This can take up to 4m0s  
> [api-check] The API server is healthy after 7.501308856s  
> [upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace  
> [kubelet] Creating a ConfigMap "kubelet-config" in namespace kube-system with the configuration for the kubelets in the cluster  
> [upload-certs] Skipping phase. Please see --upload-certs  
> [mark-control-plane] Marking the node k8s-master as control-plane by adding the labels: [node-role.kubernetes.io/control-plane node.kubernetes.io/exclude-from-external-load-balancers]  
> [mark-control-plane] Marking the node k8s-master as control-plane by adding the taints [node-role.kubernetes.io/control-plane:NoSchedule]  
> [bootstrap-token] Using token: 6akuas.njxqmbudiib0ur6v  
> [bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles  
> [bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to get nodes  
> [bootstrap-token] Configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials  
> [bootstrap-token] Configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token  
> [bootstrap-token] Configured RBAC rules to allow certificate rotation for all node client certificates in the cluster  
> [bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace  
> [kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key  
> [addons] Applied essential addon: CoreDNS  
> [addons] Applied essential addon: kube-proxy  
> 
> Your Kubernetes control-plane has initialized successfully!  
> 
> To start using your cluster, you need to run the following as a regular user:  
> 
>   mkdir -p $HOME/.kube  
>   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config  
>   sudo chown $(id -u):$(id -g) $HOME/.kube/config  
> 
> Alternatively, if you are the root user, you can run:  
> 
>   export KUBECONFIG=/etc/kubernetes/admin.conf  
> 
> You should now deploy a pod network to the cluster.  
> Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:  
>   https://kubernetes.io/docs/concepts/cluster-administration/addons/  
> 
> Then you can join any number of worker nodes by running the following on each as root:  
> 
> kubeadm join 192.168.6.7:6443 --token 6akuas.njxqmbudiib0ur6v \  
> 	--discovery-token-ca-cert-hash sha256:e6285ccc1342d1dfa362cfaa41b674fe2fd67c41ece1b78ec6de4bb6be607064  


### 18.<strong style='color: #1A97D5'> <必做> kubeadm join 加入集群</strong>

🖥️🖥️ **[vagrant@<font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**


```bash
sudo \
    kubeadm join 192.168.6.7:6443 --token 6akuas.njxqmbudiib0ur6v \
    --discovery-token-ca-cert-hash sha256:...
```


### 19.<strong style='color: #1A97D5'> <必做> Client - kubeconfig 配置文件</strong>
💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font>]$**

```bash
# - vagrant
sudo cat /etc/kubernetes/admin.conf \
  | tee /home/${LOGNAME}/.kube/config

# - root
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" \
  | sudo tee -a /root/.bashrc

```

### 20.<strong style='color: #1A97D5'> <必做> 创建网络</strong>
💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font>]$**

```bash
if ! curl --connect-timeout 2 www.google.com &>/dev/null; then
	# C. 国内
	CURL=https://raw.gitmirror.com
	DURL=https://hub.gitmirror.com/
else
	# A. 国外
	CURL=https://raw.githubusercontent.com
fi
  
# 2. ARCH
if [ "$(uname -m)" = "aarch64" ]; then 
	CLI_ARCH=arm64
else
	CLI_ARCH=amd64
fi

# 3. WGET
CILIUM_CLI_VERSION=$(curl -s ${CURL}/cilium/cilium-cli/main/stable.txt)

until curl -s -L --fail --remote-name-all ${DURL}https://github.com/cilium/cilium-cli/releases/download/${CILIUM_CLI_VERSION}/cilium-linux-${CLI_ARCH}.tar.gz; do
	sleep 1
done
  
sudo tar xvf cilium-linux-${CLI_ARCH}.tar.gz -C /usr/local/bin
rm cilium-linux-${CLI_ARCH}.tar.gz

# 4. INSTALL
cilium install --wait

cilium status

# 5. cilium_TAB
source <(cilium completion bash)

cilium completion bash \
  | sudo tee /etc/bash_completion.d/cilium >/dev/null
```

### 21.<strong style='color: #1A97D5'> [建议] Client - kubectl 命令补全</strong>
💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

> 查看帮助
>
> ```bash
> $ kubectl completion --help
> ```

```bash
# 立即生效
source <(kubectl completion bash)

# 永久生效
kubectl completion bash \
  | tee /home/${LOGNAME}/.kube/completion.bash.inc \
  | sudo tee /root/.kube/completion.bash.inc

echo 'source ~/.kube/completion.bash.inc' \
  | tee -a /home/${LOGNAME}/.bashrc \
  | sudo tee -a /root/.bashrc

```

### 22.<strong style='color: #1A97D5'> [建议] Client - kubectl 命令别名</strong>
💻🖥️🖥️ **[vagrant@<font style="color: #CE1D19">k8s-master</font> | <font style="color: #0C62B0">k8s-worker1</font> | <font style="color: #0C62B0">k8s-worker2</font>]$**

> 参考网址 https://kubernetes.io/zh-cn/docs/reference/kubectl/cheatsheet/

```bash
# 立即生效
alias k='kubectl'
complete -F __start_kubectl k

# 永久生效
cat <<-EOF \
  | tee -a /home/${LOGNAME}/.bashrc \
  | sudo tee -a /root/.bashrc
alias k='kubectl'
complete -F __start_kubectl k
EOF

```

## C. 确认环境正常

💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font>]**

```bash
$ kubectl get nodes
NAME          STATUS   ROLES           AGE    VERSION
k8s-master    Ready    control-plane   3m6s   v1.31.1
k8s-worker1   Ready    <none>          59s    v1.31.1
k8s-worker2   Ready    <none>          48s    v1.31.1

$ kubectl get pod -A
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-68cdf756d9-2k6k5   1/1     Running   0          2m35s
kube-system   calico-node-2h687                          1/1     Running   0          71s
kube-system   calico-node-cl9v8                          1/1     Running   0          2m35s
kube-system   calico-node-ggdhp                          1/1     Running   0          82s
kube-system   coredns-857d9ff4c9-hfc79                   1/1     Running   0          3m11s
kube-system   coredns-857d9ff4c9-pjwjd                   1/1     Running   0          3m11s
kube-system   etcd-k8s-master                            1/1     Running   1          3m25s
kube-system   kube-apiserver-k8s-master                  1/1     Running   1          3m25s
kube-system   kube-controller-manager-k8s-master         1/1     Running   1          3m25s
kube-system   kube-proxy-6w6d9                           1/1     Running   0          82s
kube-system   kube-proxy-szx4x                           1/1     Running   0          71s
kube-system   kube-proxy-wjqhj                           1/1     Running   0          3m11s
kube-system   kube-scheduler-k8s-master                  1/1     Running   1          3m25s

$ kubectl get componentstatuses
Warning: v1 ComponentStatus is deprecated in v1.19+
NAME                 STATUS    MESSAGE   ERROR
controller-manager   Healthy   ok
scheduler            Healthy   ok
etcd-0               Healthy   ok
```

## A. 附录

### A1. 新节点加入 K8s 集群命令

> - 查看 kubeadm init 命令的输出
> - 使用 kubeadm token create 命令重新创建

💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font>]$**

```bash
kubeadm token create --print-join-command
```



### A2. kubeadm reset 重置环境

> 还原通过==kubeadm init==或==kubeadm join==对此主机所做的更改

💻 **[vagrant@<font style="color: #CE1D19">k8s-master</font>]$**

```bash
echo y | sudo kubeadm reset
```



### A3. kubectl Err1

>  The connection to the server localhost:8080 was refused - did you specify the right host or port?

🖥️ **[vagrant@<font style="color: #0C62B0">k8s-worker2</font>]$**

```bash
# 创建目录
mkdir ~/.kube

# 拷贝配置文件
scp root@k8s-master:/etc/kubernetes/admin.conf \
    ~/.kube/config

# 验证
kubectl get node
```



### A4. RHEL布署

> - 关闭 firewalld
> - 关闭 SELinux

```bash
# >>> firewalld
systemctl disable --now firewalld

# >>> SELinux
sed -i '/^SELINUX=/s/=.*/=disabled/' \
    /etc/selinux/config
# >>> 重启生效
reboot
```



### A5. Client - Windows

> 网址 https://kubernetes.io/zh-cn/docs/tasks/tools/install-kubectl-windows/
>
> ```powershell
> curl.exe -LO "https://dl.k8s.io/release/v1.31.0/bin/windows/amd64/kubectl.exe"
> ```

- 命令

  ```powershell
  # 查看外部命令路径
  Get-ChildItem Env:
  $Env:PATH
  
  # 下载
  $Env:FURL = "http://k8s.ruitong.cn:8080/K8s/kubectl.exe"
  curl.exe -L# $Env:FURL -o $Env:LOCALAPPDATA\Microsoft\WindowsApps\kubectl.exe
  ```

- 配置文件

  ```powershell
  # 创建文件夹
  New-Item -Path $ENV:USERPROFILE -Name .kube -type directory
  
  # 拷贝配置文件
  scp vagrant@192.168.6.7:/home/vagrant/.kube/config $ENV:USERPROFILE\.kube\config
  Password: `vagrant`
  
  # 验证
  kubectl get node
  ```

- 自动补全

  > ```bash
  > kubectl.exe completion -h
  > ```

  ```bash
  # 立即生效
  kubectl completion powershell | Out-String | Invoke-Expression
  
  # 永久生效
  kubectl completion powershell > $ENV:USERPROFILE\.kube\completion.ps1
  ```
